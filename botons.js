import { firstOrAddNum, firstOrAddOp, igual} from "./funcionalidades.js"

export class Botones {


    resultado = document.getElementById('#resultado')

    cleanAll = document.querySelector('#cleanAll').onclick = () => {
        resultado.textContent = "0";
    }
    cleanOne = document.querySelector('#cleanOne').onclick = () => {
        if (resultado.textContent.length == 1) {
            resultado.textContent = "0";
        } else {
            resultado.textContent = resultado.innerHTML.slice(0, resultado.innerHTML.length - 1)
        }
    }
    divi = document.querySelector('#divi').onclick = () => {
        firstOrAddOp(resultado.innerText, resultado.innerText.length, "÷")
    }
    equal = document.querySelector('#equal').onclick = () => {

        igual()
    }
    seven = document.querySelector('#seven').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 7)
    }
    eight = document.querySelector('#eight').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 8)
    }
    nine = document.querySelector('#nine').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 9)
    }
    mult = document.querySelector('#mult').onclick = () => {
        firstOrAddOp(resultado.innerText, resultado.innerText.length, "x")
    }
    four = document.querySelector('#four').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 4)
    }
    five = document.querySelector('#five').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 5)
    }
    six = document.querySelector('#six').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 6)
    }
    subs = document.querySelector('#subs').onclick = () => {
        firstOrAddOp(resultado.innerText, resultado.innerText.length, "-")
    }
    one = document.querySelector('#one').onclick = () => {

        firstOrAddNum(resultado.innerText, resultado.innerText.length, 1)
        
    }
    two = document.querySelector('#two').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 2)
    }
    three = document.querySelector('#three').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 3)
    }
    sum = document.querySelector('#sum').onclick = () => {
        firstOrAddOp(resultado.innerText, resultado.innerText.length, "+")
    }
    parent = document.querySelector('#parent')

    zero = document.querySelector('#zero').onclick = () => {
        firstOrAddNum(resultado.innerText, resultado.innerText.length, 0)
    }

    spot = document.querySelector('#spot').onclick = () => {
          
        firstOrAddNum(resultado.innerText, resultado.innerText.length, ".")
    }    

   
    // arreglar 
    cleanResult = document.querySelector('#cleanResult')
    deri = document.querySelector('#deri')
    poten = document.querySelector('#poten')
    sqrt = document.querySelector('#sqrt')
    porcent = document.querySelector("#porcent")

}





