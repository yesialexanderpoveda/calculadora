export class Calculadora {
    constructor(){ this.result = 0; this.num1 = 0; this.num2 = 0;}
    setNumOne (num1){ this.num1 = num1;} 
    setNumTwo (num2){ this.num2 = num2;}
    getNumOne (){ return this.num1;}
    getNumTwo (){ return this.num2;} 
    getResult(){ return this.result;}
    getSum (){ this.result = this.num1 + this.num2;  this.num1 = this.result;  return this.result }
    getSubs(){ this.result = this.num1 - this.num2;  this.num1 = this.result; return this.result }
    getMult(){ this.result = this.num1 * this.num2;  this.num1 = this.result; return this.result }
    getDiv() { this.result = this.num1 / this.num2;  this.num1 = this.result;  return this.result }
    
    resetNum(){

        this.result = 0; this.num1 = 0; this.num2 = 0;
    }
    operation (op) {
        let r = 0
        if(op == '+'){
            r = this.getSum(this.num1, this.num2);
        }else if(op == '-'){
            r = this.getSubs(this.num1, this.num2);
        }else if(op == 'x'){
            r = this.getMult(this.num1, this.num2);
        }else if(op == '÷'){

            if(this.num2 == 0){
                r = 0;
            }else{
                r = this.getDiv(this.num1, this.num2);
            }
           
        }
        return r
    }
}

export let splitNumber = (numn) => {
   
    let init = "";
        numn.split('').forEach(ele => {
        if(ele === '+' || ele === '-' || ele === 'x' || ele === '÷'){
            init = numn.indexOf(`${ele}`) + 1;             
        }
    })
    numn = numn.slice(init, numn.length);    
   
    return numn;
   
}




